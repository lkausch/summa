/**
 * Logan Kausch
 * Summa Technologies
 * Associate Technical Consultant Apprenticeship Program
 * Bowling Kata v3.0
 * Score Calculator
 */

package com.summa.bowling;

import java.util.List;
import java.util.Scanner;

public class Calculator {

	public final int MAX_FRAMES = 10;
	public final int MAX_FRAME_SCORE = 10;

	public int score = 0;
	public int frame = 1;

	public void fromSheet(List<Integer> sheet) {
		int roll = 0;
		score = 0;
		frame = 1;
		while (frame < MAX_FRAMES) {
			System.out.println("Frame " + frame);
			if (isStrike(sheet.get(roll))) {
				score = scoreFrame(score, sheet.get(roll), sheet.get(roll + 1), sheet.get(roll + 2));
				roll = roll + 1;
			}
			else if (isSpare(sheet.get(roll), sheet.get(roll + 1))) {
				score = scoreFrame(score, sheet.get(roll), sheet.get(roll + 1), sheet.get(roll + 2));
				roll = roll + 2;
			}
			else {
				// Fourth variable passed is 0 because a frame w/o a strike or
				// spare can be scored w/o the next frame
				score = scoreFrame(score, sheet.get(roll), sheet.get(roll + 1), 0);
				roll = roll + 2;
			}
			frame++;
			System.out.println("Score: " + score + "\n");
		}

		System.out.println("Frame: " + MAX_FRAMES);

		if (isStrike(sheet.get(roll))) {
			score = score + MAX_FRAME_SCORE + sheet.get(roll + 1) + sheet.get(roll + 2);
			printFinalFrame(sheet.get(roll), sheet.get(roll + 1), sheet.get(roll + 2));
		}

		else if (isSpare(sheet.get(roll), sheet.get(roll + 1))) {
			score = score + MAX_FRAME_SCORE + sheet.get(roll + 2);
			printFinalFrame(sheet.get(roll), sheet.get(roll + 1), sheet.get(roll + 2));

		}

		else {
			score = score + sheet.get(roll) + sheet.get(roll + 1);
			// Third variable passed is 0 because player doesn't have a third
			// roll in the final frame w/o a strike or spare
			printFinalFrame(sheet.get(roll), sheet.get(roll + 1), 0);
		}

		System.out.println("\nFINAL SCORE: " + score);

	}

	private boolean isStrike(Integer roll) {
		return roll == MAX_FRAME_SCORE;
	}

	private boolean isSpare(Integer roll1, Integer roll2) {
		return (roll1 + roll2) == MAX_FRAME_SCORE;
	}

	private int scoreFrame(int currScore, int roll1, int roll2, int roll3) {
		if (isStrike(roll1)) {
			System.out.println("X");
			return currScore + MAX_FRAME_SCORE + roll2 + roll3;
		} else if (isSpare(roll1, roll2)) {
			System.out.println(roll1 + " /");
			return currScore + MAX_FRAME_SCORE + roll3;
		} else {
			System.out.println(roll1 + " " + roll2);
			return currScore + roll1 + roll2;
		}
	}

	private void printFinalFrame(int roll1, int roll2, int roll3) {
		if (isStrike(roll1)) {
			System.out.print("X ");
			if (isStrike(roll2)) {
				System.out.print("X ");
				if (isStrike(roll3)) {
					System.out.println("X");
				} else {
					System.out.println(roll3);
				}
			} else if (isSpare(roll2, roll3)) {
				System.out.println(roll2 + " /");
			} else {
				System.out.println(roll2 + roll3);
			}
		} else if (isSpare(roll1, roll2)) {
			System.out.print(roll1 + " /");
			if (isStrike(roll3)) {
				System.out.print("X");
			} else {
				System.out.println(roll3);
			}
		} else {
			System.out.println(roll1 + " " + roll2);
		}
	}

	public void liveScoring() {

		int roll1 = 0;
		int roll2 = 0;
		score = 0;
		frame = 1;

		System.out.println("Enter each roll, followed by the return key.\n");

		while (frame < MAX_FRAMES) {
			
			System.out.println("Frame " + frame);

			@SuppressWarnings("resource")
			Scanner reader = new Scanner(System.in);
			roll1 = reader.nextInt();

			if (isStrike(roll1)) {
				score = score + MAX_FRAME_SCORE + scoreStrike();
				System.out.println("Score: " + score);
				frame++;
			}
			else {
				roll2 = reader.nextInt();
				if (isSpare(roll1, roll2)) {
					score = score + MAX_FRAME_SCORE + scoreSpare();
					System.out.println("Score: " + score);
					frame++;
				} else {
					score = score + roll1 + roll2;
					System.out.println("Score: " + score);
					frame++;
				}
			}
		}

		System.out.println("FINAL SCORE: " + score);
	}

	private int scoreStrike() {
		System.out.println("Frame " + frame);
		
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);

		int roll1 = reader.nextInt();

		if (isStrike(roll1)) {
			frame++;
			return 2*MAX_FRAME_SCORE + scoreStrike();
		}

		else {
			int roll2 = reader.nextInt();
			if (isSpare(roll1, roll2)) {
				frame++;
				return 2 * MAX_FRAME_SCORE + scoreSpare();
			} else {
				frame++;
				return 2 * (roll1 + roll2);
			}
		}
	}

	private int scoreSpare() {
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);

		int roll1 = reader.nextInt();
		int roll2 = 0;

		System.out.println("Frame " + frame);

		if (isStrike(roll1)) {
			frame++;
			return MAX_FRAME_SCORE + scoreStrike();
		}

		else {
			roll2 = reader.nextInt();
			if (isSpare(roll1, roll2)) {
				frame++;
				return MAX_FRAME_SCORE + roll1 + scoreSpare();
			} else {
				frame++;
				return 2 * roll1 + roll2;
			}
		}
	}

}
