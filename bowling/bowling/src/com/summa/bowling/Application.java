/**
 * Logan Kausch
 * Summa Technologies
 * Associate Technical Consultant Apprenticeship Program
 * Bowling Kata v3.0
 * Main Application
 */

package com.summa.bowling;

import java.io.IOException;

public class Application {

	public static void main(String[] args) throws IOException {
		BowlingGame bowling = new BowlingGame();
		bowling.start();

	}

}