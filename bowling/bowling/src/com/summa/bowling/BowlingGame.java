/**
 * Logan Kausch
 * Summa Technologies
 * Associate Technical Consultant Apprenticeship Program
 * Bowling Kata v3.0
 * Bowling Game
 */

package com.summa.bowling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BowlingGame {

	public static final int MAX_ROLLS = 21;

	public void start() throws IOException {

		ArrayList<Integer> rolls = new ArrayList<>();
		Calculator calculator = new Calculator();
		boolean again = true;

		while (again) {
			int choice = welcome();

			switch (choice) {
			case 1:
				calculator.fromSheet(hardcodeSheet(rolls));
				break;
			case 2:
				calculator.fromSheet(readSheet());
				break;
			case 3:
				calculator.fromSheet(manualEntry(rolls));
				break;
			case 4:
				calculator.liveScoring();
				System.exit(0);
			default:
				System.out.println("Not a valid response.\n");
			}
			again = play();
		}

	}

	private int welcome() {
		System.out.println("Welcome to the Bowling Calculator!");
		System.out.println("Select a method to choose a score sheet:");
		System.out.println("1: Hardcoded\n2: Text file\n3: Manual entry\n4: Live scoring");

		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		System.out.println("Method: ");
		int choice = reader.nextInt();
		return choice;
	}

	private List<Integer> hardcodeSheet(List<Integer> rolls) {
		Integer[] sheet = new Integer[] { 7, 3, 3, 2, 5, 5, 3, 0, 10, 10, 7, 3, 10, 6, 2, 10, 9, 1 };
		return Arrays.asList(sheet);
	}

	private List<Integer> readSheet() throws IOException {
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter the name of the file.\n");
		String file = reader.nextLine();
		return readScores(file);

	}

	private List<Integer> readScores(String f) throws IOException {
		ArrayList<Integer> list = new ArrayList<>();

		BufferedReader reader = new BufferedReader(new FileReader(f));

		while (true) {
			String line = reader.readLine();
			if (line == null) {
				break;
			}
			list.add(Integer.parseInt(line));
		}
		reader.close();

		return list;

	}

	private List<Integer> manualEntry(ArrayList<Integer> rolls) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter each roll followed by the return key. When complete, enter a non-integer value.\n");
		while (reader.hasNextInt()) {
			rolls.add(reader.nextInt());
		}
		reader.close();
		return rolls;
	}

	private boolean play() {
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		System.out.println("Would you like to enter another sheet? (y/n): ");
		String choice = reader.next();
		if (choice.equals("y") || choice.equals("Y")) {
			return true;
		} else {
			return false;
		}
	}

}
